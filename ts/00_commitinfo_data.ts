/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartbucket',
  version: '2.0.2',
  description: 'simple cloud independent object storage'
}
